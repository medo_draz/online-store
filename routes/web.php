<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/product/{slug}', [App\Http\Controllers\HomeController::class, 'productDetails'])->name('product.details');
Route::get('/product/show/modal/{slug}', [App\Http\Controllers\HomeController::class, 'showModel'])->name('product.show');
Route::get('/product/right_card/show/{type}', [App\Http\Controllers\HomeController::class, 'right_card']);
Route::middleware(['auth'])->post('/product/favourite/add', [App\Http\Controllers\HomeController::class, 'addFavourite']);
Route::get('/about', [App\Http\Controllers\HomeController::class, 'about'])->name('about');
Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact'])->name('contact');
Route::get('/blog', [App\Http\Controllers\HomeController::class, 'blog'])->name('blog');
Route::get('/category/{category}', [App\Http\Controllers\HomeController::class, 'category'])->name('category');

Auth::routes();

Route::get('/home', function (){
    return view('home');
});


Route::get('social-share', [App\Http\Controllers\HomeController::class, 'index1']);

Route::prefix('admin')->name('admin.')->middleware(['auth','isAdmin'])->group( function () {
    Route::get('/home', [App\Http\Controllers\Dashboard\HomeController::class, 'index'])->name('home');
    Route::resource('categories', App\Http\Controllers\Dashboard\CategoriesController::class);
    Route::resource('products', App\Http\Controllers\Dashboard\ProductController::class);
});

