<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = collect(User::where('isAdmin', 1)->get()->modelKeys());
        $categories = ['Women', 'Men', 'Bag', 'Shoes', 'Watches'];
        $images = ['banner-01.jpg', 'banner-02.jpg', 'banner-08.jpg', 'banner-10.jpg', 'banner-07.jpg'];
        foreach ($categories as $index=>$category){
            Category::create([
                'name' => $category,
                'image' => $images[$index],
                'created_by'   => $user->random(),
            ]);
        }
    }
}
