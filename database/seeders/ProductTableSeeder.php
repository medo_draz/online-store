<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $posts = [];
        $categories = collect(Category::all()->modelKeys());
        $user = collect(User::where('isAdmin', 1)->get()->modelKeys());

        for ($i = 0; $i < 500; $i++) {
            $days = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28'];
            $months = ['01', '02', '03', '04', '05', '06', '07', '08'];
            $post_date = "2020-" . Arr::random($months) . "-" . Arr::random($days) . " 01:01:01";
            $post_title = $faker->sentence(mt_rand(3, 5), true);
            $price = rand(300, 1000);

            $posts[] = [
                'name'         => $post_title,
                'slug'          => Str::slug($post_title),
                'desc'   => $faker->paragraph(30),
                'status'        => rand(0, 1),
                'stock'  => rand(1, 200),
                'price'  => $price,
                'sale_price'  => $price+50,
                'disc_price'  => $price+50,
                'weight'   => rand(40, 100),
//                'image' => $faker->image('public/frontend/images/products',1200,1486, 'cats'),
                'materials'   => $faker->sentence(mt_rand(3, 5), true),
                'category_id'   => $categories->random(),
                'created_by'   => $user->random(),
                'created_at'    => $post_date,
                'updated_at'    => $post_date,

            ];
        }

        $chunks = array_chunk($posts, 100);
        foreach ($chunks as $chunk) {
           Product::insert($chunk);
        }

    }
}
