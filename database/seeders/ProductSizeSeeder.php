<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProductSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sizes = collect(['S', 'M', 'L', 'XL', 'XXL', 'XXXL']);
        $colors = collect(['Black', 'Green', 'Red', 'Blue', 'White', 'Grey']);
        $user = collect(User::where('isAdmin', 1)->get()->modelKeys());
        $products = Product::all();
        foreach ($products as $product) {
            $max = rand(3, 6);
            $created_at = $user->random();
            foreach ($sizes as $size) {
                $product_sizes = ProductSize::where('product_id', $product->id)->get();
                $product_size = ProductSize::where('product_id', $product->id)->where('size', $size)->first();
                if (empty($product_size) && $max != count($product_sizes)) {
                    ProductSize::create([
                        'product_id' => $product->id,
                        'size' => $size,
                        'created_by'   => $created_at,
                    ]);
                }
            }
            foreach ($colors as $color) {
                $product_sizes = ProductColor::where('product_id', $product->id)->get();
                $product_size = ProductColor::where('product_id', $product->id)->where('color', $color)->first();
                if (empty($product_size) && $max != count($product_sizes)) {
                    ProductColor::create([
                        'product_id' => $product->id,
                        'color' => $color,
                        'created_by'   => $created_at,
                    ]);
                }
            }
        }
    }
}
