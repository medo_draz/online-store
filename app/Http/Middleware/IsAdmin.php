<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //If the status is not approved redirect to login
//        dd(Auth::user());
        if(Auth::check() && Auth::user()->isAdmin != 1){
            Auth::logout();
            return redirect('/login')->with('status_msg', 'Your acount  is Deactive');
        }
        return $next($request);
    }
}
