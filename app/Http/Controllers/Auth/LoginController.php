<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function login(Request $request)
    {
        if (!empty($request)) {
            $details = '';
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
                Auth::login($user);
//                dd($user->isAdmin);
                if ($user->isAdmin === 1) {
                    Session::put('user_type', 'admin');
//                    $this->skollog(['component'=>'users','action'=>'login','req'=>$request->all(),'res'=>200,'data'=>'Login success']);
                    return redirect('/admin/home');
                } else {
                    Session::put('user_type', 'user');
//                    $this->skollog(['component'=>'users','action'=>'login','req'=>$request->all(),'res'=>200,'data'=>'Login success']);
                    return redirect('/');
                }
            } else {
                return Redirect::back()->withErrors(['Login Field', 'check your information and try again ']);
            }
        }
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
