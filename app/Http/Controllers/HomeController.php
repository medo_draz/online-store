<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ProductFavourite;
use App\Models\ProductOrder;
use Illuminate\Http\Request;
use App\Models\Product;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//         $this->middleware('auth')->only('addFavourite');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        $favourites = ProductFavourite::pluck('product_id')->toArray();
//        $cards = ProductOrder::pluck('product_id')->toArray();
//        dd($favourites);
        $products = Product::with(['category', 'colors', 'sizes'])->where('status', 1)->latest()->paginate(16);
        return view('index', compact('products', 'favourites', 'categories'));
    }

    public function category($category)
    {
//        dd($category);
        $categories = Category::all();
        $favourites = ProductFavourite::pluck('product_id')->toArray();
//        $cards = ProductOrder::pluck('product_id')->toArray();
//        dd($favourites);
        $products = Product::with(['category', 'colors', 'sizes'])->where('status', 1)
            ->whereHas('category', function ($q) use ($category) {
//                return $q->where('name', $category);
            })->latest()->paginate(16);
        return view('category', compact('products', 'category', 'favourites', 'categories'));
    }

    public function productDetails($slug)
    {
        $product = Product::with(['category', 'colors', 'sizes'])->where('status', 1)->where('slug', $slug)->first();
        $favourites = ProductFavourite::pluck('product_id')->toArray();
//        $cards = ProductOrder::pluck('product_id')->toArray();
        $related_products = Product::with(['category', 'colors', 'sizes'])->whereHas('category', function ($q) use ($product) {
            return $q->where('name', $product->category->name);
        })->where('id', '!=', $product->id)->where('status', 1)->paginate(8);
        return view('product', compact('product', 'favourites', 'related_products'));
    }

    public function showModel($slug)
    {
        $product_detail = Product::with(['category', 'colors', 'sizes'])->where('status', 1)->where('slug', $slug)->first();
        $view = view('layouts.product_details', compact('product_detail'))->render();
//        dd($product);
        return response()->json(['html' => $view]);
//        return view('layouts.product_details',compact('product_detail'));
    }

    public function right_card($type)
    {
//        dd($type);
        if ($type == 'favourite') {
            $products = ProductFavourite::with(['product'])->get();
        } else if ($type == 'card') {
            $products = ProductOrder::with(['product'])->get();
        }
//        $product_detail = Product::with(['category', 'colors', 'sizes'])->where('status', 1)->where('slug', $slug)->first();
//        $view = view('layouts.product_details', compact('product_detail'))->render();
////        dd($product);
//        return response()->json(['html' => $view]);
        return view('layouts.sidecartBody', compact('products', 'type'));
    }

    public function about()
    {
        $favourites = ProductFavourite::pluck('product_id')->toArray();
//        $cards = ProductOrder::pluck('product_id')->toArray();
        return view('about', compact('favourites'));
    }

    public function contact()
    {
        $favourites = ProductFavourite::pluck('product_id')->toArray();
//        $cards = ProductOrder::pluck('product_id')->toArray();
        return view('contact', compact('favourites'));
    }

    public function blog()
    {
        $favourites = ProductFavourite::pluck('product_id')->toArray();
//        $cards = ProductOrder::pluck('product_id')->toArray();
        return view('blog', compact('favourites'));
    }

    public function addFavourite(Request $request)
    {
//        dd($request->prod_id);
        $product = ProductFavourite::where('product_id', $request->prod_id)->first();
        if ($product) {
            $product->delete();
            return response()->json(['error' => false, 'type' => 'delete', 'message' => 'Product Remove Favourite Successful', 'data' => [], 200]);
        } else {
            ProductFavourite::create([
                'product_id' => $request->prod_id,
                'user_id' => Auth::user()->id,
            ]);
            return response()->json(['error' => false, 'type' => 'add', 'message' => 'Product Add Favourite Successful', 'data' => [], 200]);
        }
    }

    public function index1()
    {
        $shareButtons = \Share::page(
            'https://www.itsolutionstuff.com',
            'Your share text comes here',
        )
            ->facebook()
            ->twitter()
            ->linkedin()
            ->telegram()
            ->whatsapp()
            ->reddit();

        $posts = Product::get();

        return view('socialshare', compact('shareButtons', 'posts'));
    }
}
