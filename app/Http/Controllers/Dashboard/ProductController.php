<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductImage;
use App\Models\ProductSize;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::with(['category','user','images'])->latest()->paginate(10);
        return view('admin.products.index',compact('products'));
    }


    public function create()
    {
        $categories = Category::all();
        return view('admin.products.create',compact('categories'));
    }


    public function store(Request $request)
    {
//        dd($request);

        $user_id = Auth::user()->id;
        DB::beginTransaction();
        $product = new Product();
        $product->name = $request->name;
        $product->category_id = $request->category_id;
        $product->desc = $request->desc;
        $product->materials = $request->materials;
        $product->price = $request->price;
        $product->sale_price = $request->sale_price;
        $product->stock = $request->stock;
        $product->discount = $request->discount;
        $product->weight = 80;
        if ($request->discount) {
            $product->disc_price = $request->sale_price - ((double)($request->sale_price) * ((int)$request->discount/100));
        } else {
            $product->disc_price = $request->sale_price;
        }
        if (isset($request->status)){
            $product->status = 1;
        } else {
            $product->status = 0;
        }
        $product->created_by = $user_id;
        if($request->hasfile('image')){
            Image::make($request->image)
                ->resize(1200, 779, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('frontend/images/products/' . $request->image->hashName()));
            $product->image = $request->image->hashName();
        }
        $product->save();
        if($request->sizes) {
            foreach ($request->sizes as $size) {
                ProductSize::create([
                    'product_id'=> $product->id,
                    'size'=> $size,
                    'created_by'=> $user_id
                ]);
            }
        }
        if($request->colors) {
            foreach ($request->colors as $color) {
                ProductColor::create([
                    'product_id'=> $product->id,
                    'color'=> $color,
                    'created_by'=> $user_id
                ]);
            }
        }
        if($request->hasfile('images')){
            foreach($request->file('images') as $image)
            {
                $product_image = new ProductImage();
                $product_image->product_id = $product->id;
                Image::make($image)
                    ->resize(1200, 779, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->save(public_path('frontend/images/products/' . $image->hashName()));
                $product_image->image = $image->hashName();
                $product_image->save();
            }
        }
        DB::commit();
        session()->flash('success', 'Product Added Successfully');
        return redirect()->route('admin.products.index');
    }


    public function show(Product $product)
    {
//        dd($product);
    }


    public function edit(Product $product)
    {
//        dd($product);
        $categories = Category::all();
        $product_colors = ProductColor::where('product_id', $product->id)->pluck('color')->toArray();
        $product_sizes = ProductSize::where('product_id', $product->id)->pluck('size')->toArray();
//        dd($product_colors);
        return view('admin.products.create',compact('product','categories','product_colors','product_sizes'));
    }


    public function update(Request $request, Product $product)
    {
//        dd($product,'sdfvcdx');
        $user_id = Auth::user()->id;
        DB::beginTransaction();
//        $product = new Product();
        $product->name = $request->name;
        $product->category_id = $request->category_id;
        $product->desc = $request->desc;
        $product->materials = $request->materials;
        $product->price = $request->price;
        $product->sale_price = $request->sale_price;
        $product->stock = $request->stock;
        $product->discount = $request->discount;
        $product->weight = 80;
        if ($request->discount) {
            $product->disc_price = $request->sale_price - ((double)($request->sale_price) * ((int)$request->discount/100));
        } else {
            $product->disc_price = $request->sale_price;
        }
        if (isset($request->status)){
            $product->status = 1;
        } else {
            $product->status = 0;
        }
        $product->updated_by = $user_id;
        if($request->hasfile('image')){
            if ($product->image != 'default.png') {
                File::delete(public_path('/frontend/images/products/'.$product->image));
            }//end of if
            Image::make($request->image)
                ->resize(1200, 779, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('frontend/images/products/' . $request->image->hashName()));
            $product->image = $request->image->hashName();
        }
        $product->save();
        if($request->sizes) {
            $product_sizes = ProductSize::where('product_id', $product->id)->get();
            if (count($product_sizes) > 0) {
                foreach ($product_sizes as $product_size) {
                    $product_size->delete();
                }
            }
            foreach ($request->sizes as $size) {
                ProductSize::create([
                    'product_id'=> $product->id,
                    'size'=> $size,
                    'created_by'=> $user_id
                ]);
            }
        }
        if($request->colors) {
            $product_colors = ProductColor::where('product_id', $product->id)->get();
            if (count($product_colors) > 0) {
                foreach ($product_colors as $product_color) {
                    $product_color->delete();
                }
            }
            foreach ($request->colors as $color) {
                ProductColor::create([
                    'product_id'=> $product->id,
                    'color'=> $color,
                    'created_by'=> $user_id
                ]);
            }
        }
        if($request->hasfile('images')){
            $product_images = ProductImage::where('product_id', $product->id)->get();
            if (count($product_images) > 0) {
                foreach ($product_images as $product_image) {
                    File::delete(public_path('/frontend/images/products/'.$product_image->image));
                    $product_image->delete();
                }
            }
            foreach($request->file('images') as $image)
            {
                $product_image = new ProductImage();
                $product_image->product_id = $product->id;
                Image::make($image)
                    ->resize(1200, 779, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->save(public_path('frontend/images/products/' . $image->hashName()));
                $product_image->image = $image->hashName();
                $product_image->save();
            }
        }
        DB::commit();
        session()->flash('success', 'Product Added Successfully');
        return redirect()->route('admin.products.index');
    }


    public function destroy(Request $request, Product $product)
    {
        $product1 = Product::findOrFail($request->id);
        $product_images = ProductImage::where('product_id', $product1->id)->get();
//        dd($product_images);
        DB::beginTransaction();
        if ($product1->image != 'default.png') {
            File::delete(public_path('/frontend/images/products/'.$product1->image));
        }//end of if
        $product1->delete();
        if (count($product_images) > 0) {
            foreach ($product_images as $product_image) {
                File::delete(public_path('/frontend/images/products/'.$product_image->image));
                $product_image->delete();
            }
        }
        DB::commit();
        session()->flash('success', 'Product Deleted Successfully');
        return redirect()->route('admin.products.index');
    }
}
