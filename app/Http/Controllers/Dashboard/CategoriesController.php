<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CategoriesController extends Controller
{

    public function index()
    {
        $categories = Category::with(['user'])->get();
        return view('admin.categories.index',compact('categories'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
//        dd($request);
        $category = new Category();
        $category->name = $request->name;
        if ($request->image) {
//            dd($request);
            Image::make($request->image)
                ->resize(1200, 779, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('frontend/images/categories/' . $request->image->hashName()));
            $category->image = $request->image->hashName();
        }//end of if
        $category->save();
        session()->flash('success', 'Category Added Successfully');
        return redirect()->route('admin.categories.index');
    }


    public function show(Category $category)
    {
        //
    }


    public function edit(Category $category)
    {
        //
    }


    public function update(Request $request, Category $category)
    {
//        dd($request);
        $category = Category::where('id', $request->id)->first();
        $category->name = $request->name;
        if ($request->image) {
//            dd(Storage::disk('public_uploads'));
            if ($category->image != 'default.png') {
//                dd(File::delete(public_path('/frontend/images/'.$category->image)));
                File::delete(public_path('/frontend/images/categories/'.$category->image));
//                Storage::disk('public')->delete('/frontend/images/'.$category->image);
            }//end of if
            Image::make($request->image)
                ->resize(1200, 779, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('frontend/images/categories/' . $request->image->hashName()));
            $category->image = $request->image->hashName();
        }//end of if
        $category->save();
        session()->flash('success', 'Category Updated Successfully');
        return redirect()->route('admin.categories.index');
    }


    public function destroy(Request $request, Category $category)
    {
//        dd($category);
        $category1 = Category::findOrFail($request->id);
        if ($category1->image != 'default.png') {
//                dd(File::delete(public_path('/frontend/images/'.$category->image)));
            File::delete(public_path('/frontend/images/categories/'.$category1->image));
//                Storage::disk('public')->delete('/frontend/images/'.$category->image);
        }//end of if
//        dd($category1);
        $category1->delete();
        session()->flash('success', 'Category Deleted Successfully');
        return redirect()->route('admin.categories.index');
    }
}
