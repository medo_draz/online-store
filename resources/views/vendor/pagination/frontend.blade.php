@if ($paginator->hasPages())
    <div class="flex-c-m flex-w w-full p-t-38">
        {{--        <ul class="flex-c-m flex-w w-full p-t-38">--}}
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            {{--                <li class="flex-c-m how-pagination1 trans-04 m-all-7 disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">--}}
            <span class="flex-c-m how-pagination1 trans-04 m-all-7 disabled" aria-hidden="true">&lsaquo;</span>
            {{--                </li>--}}
        @else
            {{--                <li class="flex-c-m how-pagination1 trans-04 m-all-7 ">--}}
            <a class="flex-c-m how-pagination1 trans-04 m-all-7" href="{{ $paginator->previousPageUrl() }}" rel="prev"
               aria-label="@lang('pagination.previous')">&lsaquo;</a>
            {{--                </li>--}}
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                {{--                    <li class="flex-c-m how-pagination1 trans-04 m-all-7 disabled" aria-disabled="true">--}}
                <span class="flex-c-m how-pagination1 trans-04 m-all-7 disabled">{{ $element }}</span>
                {{--                    </li>--}}
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        {{--                            <li class="flex-c-m how-pagination1 trans-04 m-all-7 active-pagination1" aria-current="page">--}}
                        <span class="flex-c-m how-pagination1 trans-04 m-all-7 active-pagination1">{{ $page }}</span>
                        {{--                            </li>--}}
                    @else
                        {{--                            <li class="flex-c-m how-pagination1 trans-04 m-all-7 ">--}}
                        <a class="flex-c-m how-pagination1 trans-04 m-all-7" href="{{ $url }}">{{ $page }}</a>
                        {{--                            </li>--}}
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            {{--                <li class="flex-c-m how-pagination1 trans-04 m-all-7 ">--}}
            <a class="flex-c-m how-pagination1 trans-04 m-all-7" href="{{ $paginator->nextPageUrl() }}" rel="next"
               aria-label="@lang('pagination.next')">&rsaquo;</a>
            {{--                </li>--}}
        @else
            {{--                <li class="flex-c-m how-pagination1 trans-04 m-all-7 disabled" aria-disabled="true" aria-label="@lang('pagination.next')">--}}
            <span class="flex-c-m how-pagination1 trans-04 m-all-7 disabled" aria-hidden="true">&rsaquo;</span>
            {{--                </li>--}}
            @endif
            </ul>
    </div>
@endif
