@extends('layouts.dashboard.master')
@section('css')
    <link href="{{URL::asset('dashboard/assets/plugins/owl-carousel/owl.carousel.css')}}" rel="stylesheet" />
    <!-- Maps css -->
    <link href="{{URL::asset('dashboard/assets/plugins/jqvmap/jqvmap.min.css')}}" rel="stylesheet">
@section('title')
    Categories - Admin Panel
@endsection
<!-- fgdsgd -->
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong style="margin-right: 30px;">{{ session()->get('success') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">


        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
{{--                     @can('اضافة قسم')--}}
                        <a class="modal-effect btn btn-outline-primary btn-block" data-effect="effect-scale"
                           data-toggle="modal" style="width: auto;" href="#modaldemo8">Add Category</a>
{{--                     @endcan--}}
                    </div>
{{--                    <a class="modal-effect btn btn-outline-primary btn-block" data-effect="effect-scale"--}}
{{--                       data-toggle="modal" href="#modaldemo8" style="width: 105px;">Add Category</a>--}}

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example1" class="table key-buttons text-md-nowrap" data-page-length='50'
                               style="text-align: center">
                            <thead>
                            <tr>
                                <th class="border-bottom-0">#</th>
                                <th class="border-bottom-0">Category Name</th>
                                <th class="border-bottom-0">Image</th>
                                <th class="border-bottom-0">Created By</th>
                                <th class="border-bottom-0">Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($categories as $index => $category)

                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td><img src="{{ asset('frontend/images/'.$category->image) }}" style="width: 85px" class="img-thumbnail" alt="">
                                    </td>
                                    <td>{{ $category->user->name }}</td>

                                    <td>
                                        {{--                                        @can('تعديل قسم')--}}
                                        <a class="modal-effect btn btn-sm btn-info" data-effect="effect-scale"
                                           data-id="{{ $category->id }}" data-product_name="{{ $category->name }}"
                                           data-image="{{ asset('frontend/images/'.$category->image) }}"
                                           data-toggle="modal"
                                           href="#edit_Product" title="تعديل"><i class="las la-pen"></i></a>
                                        {{--                                        @endcan--}}

                                        {{--                                        @can('حذف قسم')--}}
                                        <a class="modal-effect btn btn-sm btn-danger" data-effect="effect-scale"
                                           data-id="{{ $category->id }}" data-product_name="{{ $category->name }}"
                                           data-toggle="modal" href="#modaldemo9" title="حذف"><i
                                                class="las la-trash"></i></a>
                                        {{--                                        @endcan--}}
                                    </td>
                                </tr>
                                <!-- edit -->
                                <div class="modal fade" id="edit_Product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                <form action="{{ route('admin.categories.update',$category) }}" method="post" enctype="multipart/form-data" autocomplete="off">
                                                    {{ method_field('put') }}
                                                    {{ csrf_field() }}
                                                    <div class="form-group" style="display: flex;">
                                                        <input type="hidden" name="id" id="pro_id" value="">
                                                        <label for="recipient-name" class="col-form-label" style="width: 155px;">Category Name : </label>
                                                        <input class="form-control" name="name" id="product_name" type="text">
                                                    </div>
                                                    <div class="form-group" style="display: flex;">
                                                        <label for="message-text" class="col-form-label" style="width: 155px;">Image : </label>
                                                        <input type="file" name="image" style="min-height: 42px !important;" class="form-control image">
                                                    </div>
                                                    <div class="form-group">
                                                        <img src="{{ asset('frontend/images/products/default.jpg') }}" id="image-preview" style="width: 100px" class="img-thumbnail image-preview" alt="">
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">تاكيد</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- delete -->
                                <div class="modal" id="modaldemo9">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content modal-content-demo">
                                            <div class="modal-header">
                                                <h6 class="modal-title">حذف المنتج</h6>
                                                <button aria-label="Close" class="close" data-dismiss="modal"
                                                        type="button"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <form action="{{ route('admin.categories.destroy',$category) }}" method="post">
                                                {{ method_field('delete') }}
                                                {{ csrf_field() }}
                                                <div class="modal-body">
                                                    <p>هل انت متاكد من عملية الحذف ؟</p><br>
                                                    <input type="hidden" name="id" id="pro_id" value="">
                                                    <input class="form-control" name="product_name" id="product_name" type="text" readonly>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                                                    <button type="submit" class="btn btn-danger">تاكيد</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="modaldemo8">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Add Category</h6>
                        <button aria-label="Close" class="close" data-dismiss="modal"
                                type="button"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('admin.categories.store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('post') }}

                            <div class="form-group" style="display: flex;">
                                <input type="hidden" name="id" id="pro_id" value="">
                                <label for="recipient-name" class="col-form-label" style="width: 155px;">Category Name : </label>
                                <input class="form-control" name="name" id="product_name" type="text">
                            </div>
                            <div class="form-group" style="display: flex;">
                                <label for="message-text" class="col-form-label" style="width: 155px;">Image : </label>
                                <input type="file" name="image" style="min-height: 42px !important;" class="form-control image">
                            </div>
                            <div class="form-group">
                                <img src="{{ asset('frontend/images/products/default.jpg') }}" id="image-preview" style="width: 100px" class="img-thumbnail image-preview" alt="">
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">تاكيد</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End Basic modal -->
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            setTimeout(function () {
                $(".alert-success").fadeOut(1500);
            }, 3000)
            setTimeout(function () {
                $(".alert-danger").fadeOut(1500);
            }, 3000)

        })
        $('#edit_Product').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var Product_name = button.data('product_name')
            var section_name = button.data('section_name')
            var pro_id = button.data('id')
            var image = button.data('image')
            var modal = $(this)
            modal.find('.modal-body #product_name').val(Product_name);
            modal.find('.modal-body #section_name').val(section_name);
            {{--modal.find('.modal-body #image-preview').src = {{ asset('frontend/images/') }}+image;--}}
            document.getElementById("image-preview").src = image;
            // console.log(image)
            modal.find('.modal-body #pro_id').val(pro_id);
        })
        $('#modaldemo9').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var pro_id = button.data('id')
            var product_name = button.data('product_name')
            var modal = $(this)
            modal.find('.modal-body #pro_id').val(pro_id);
            modal.find('.modal-body #product_name').val(product_name);
        })
    </script>

@endsection
