@extends('layouts.dashboard.master')
@section('css')
    <link href="{{URL::asset('dashboard/assets/plugins/owl-carousel/owl.carousel.css')}}" rel="stylesheet" />
    <!-- Maps css -->
    <link href="{{URL::asset('dashboard/assets/plugins/jqvmap/jqvmap.min.css')}}" rel="stylesheet">
@section('title')
    Products - Admin Panel
@endsection
<!-- fgdsgd -->
@endsection
@section('page-header')
    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto">الاعدادات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/
                المنتجات</span>
            </div>
        </div>
    </div>
    <!-- breadcrumb -->
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong style="margin-right: 30px;">{{ session()->get('success') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row">


        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
{{--                     @can('اضافة قسم')--}}
                        <a class="btn btn-outline-primary btn-block" style="width: auto;" href="{{ route('admin.products.create') }}">Add Product</a>
{{--                     @endcan--}}
                    </div>
{{--                    <a class="modal-effect btn btn-outline-primary btn-block" data-effect="effect-scale"--}}
{{--                       data-toggle="modal" href="#modaldemo8" style="width: 105px;">Add Category</a>--}}

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover key-buttons text-md-nowrap" data-page-length='10'
                               style="text-align: center">
                            <thead>
                            <tr>
                                <th class="border-bottom-0">#</th>
                                <th class="border-bottom-0">Product Name</th>
                                <th class="border-bottom-0">Category Name</th>
                                <th class="border-bottom-0">Image</th>
                                <th class="border-bottom-0">Created By</th>
                                <th class="border-bottom-0">Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($products as $index => $product)

                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->category->name }}</td>
                                    <td>
                                        @if($product->image)
                                        <img src="{{ asset('frontend/images/products/'.$product->image) }}" style="width: 85px;height: 70px;" class="img-thumbnail" alt="">
                                        @else
                                            <img src="{{ asset('frontend/images/products/default.jpg') }}" style="width: 70px;height: 70px;" class="img-thumbnail" alt="">
                                        @endif
                                    </td>
                                    <td>{{ $product->user->name }}</td>

                                    <td>
                                        {{--                                        @can('تعديل قسم')--}}
                                        <a class="btn btn-sm btn-info" href="{{ route('admin.products.edit',$product) }}"><i class="las la-pen"></i></a>
                                        {{--                                        @endcan--}}

                                        {{--                                        @can('حذف قسم')--}}
                                        <a class="modal-effect btn btn-sm btn-danger" data-effect="effect-scale"
                                           data-id="{{ $product->id }}" data-product_name="{{ $product->name }}"
                                           data-toggle="modal" href="#modaldemo9" title="حذف"><i
                                                class="las la-trash"></i></a>
                                        {{--                                        @endcan--}}
                                    </td>
                                </tr>
                                <!-- edit -->
                                <div class="modal fade" id="edit_Product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                <form action="{{ route('admin.products.update',$product) }}" method="post" enctype="multipart/form-data" autocomplete="off">
                                                    {{ method_field('put') }}
                                                    {{ csrf_field() }}
                                                    <div class="form-group" style="display: flex;">
                                                        <input type="hidden" name="id" id="pro_id" value="">
                                                        <label for="recipient-name" class="col-form-label" style="width: 155px;">Category Name : </label>
                                                        <input class="form-control" name="name" id="product_name" type="text">
                                                    </div>
                                                    <div class="form-group" style="display: flex;">
                                                        <label for="message-text" class="col-form-label" style="width: 155px;">Image : </label>
                                                        <input type="file" name="image" style="min-height: 42px !important;" class="form-control image">
                                                    </div>
                                                    <div class="form-group">
                                                        <img src="{{ asset('frontend/images/products/default.jpg') }}" id="image-preview" style="width: 100px" class="img-thumbnail image-preview" alt="">
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">تاكيد</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- delete -->
                                <div class="modal" id="modaldemo9">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content modal-content-demo">
                                            <div class="modal-header">
                                                <h6 class="modal-title">Delete Product</h6>
                                                <button aria-label="Close" class="close" data-dismiss="modal"
                                                        type="button"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <form action="{{ route('admin.products.destroy',$product) }}" method="post">
                                                {{ method_field('delete') }}
                                                {{ csrf_field() }}
                                                <div class="modal-body">
                                                    <p>Are Sure of the Deleting Process ?</p><br>
                                                    <input type="hidden" name="id" id="pro_id" value="">
                                                    <input class="form-control" name="product_name" id="product_name" type="text" readonly>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $products->appends(request()->input())->links() !!}
                </div>
            </div>
        </div>
        <div class="modal" id="modaldemo8">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Add Category</h6>
                        <button aria-label="Close" class="close" data-dismiss="modal"
                                type="button"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('admin.products.store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('post') }}

                            <div class="form-group" style="display: flex;">
                                <input type="hidden" name="id" id="pro_id" value="">
                                <label for="recipient-name" class="col-form-label" style="width: 155px;">Category Name : </label>
                                <input class="form-control" name="name" id="product_name" type="text">
                            </div>
                            <div class="form-group" style="display: flex;">
                                <label for="message-text" class="col-form-label" style="width: 155px;">Image : </label>
                                <input type="file" name="image" style="min-height: 42px !important;" class="form-control image">
                            </div>
                            <div class="form-group">
                                <img src="{{ asset('frontend/images/products/default.jpg') }}" id="image-preview" style="width: 100px" class="img-thumbnail image-preview" alt="">
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">تاكيد</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End Basic modal -->
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            setTimeout(function () {
                $(".alert-success").fadeOut(1500);
            }, 3000)
            setTimeout(function () {
                $(".alert-danger").fadeOut(1500);
            }, 3000)

        })
        $('#edit_Product').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var Product_name = button.data('product_name')
            var section_name = button.data('section_name')
            var pro_id = button.data('id')
            var image = button.data('image')
            var modal = $(this)
            modal.find('.modal-body #product_name').val(Product_name);
            modal.find('.modal-body #section_name').val(section_name);
            {{--modal.find('.modal-body #image-preview').src = {{ asset('frontend/images/') }}+image;--}}
            document.getElementById("image-preview").src = image;
            // console.log(image)
            modal.find('.modal-body #pro_id').val(pro_id);
        })
        $('#modaldemo9').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var pro_id = button.data('id')
            var product_name = button.data('product_name')
            var modal = $(this)
            modal.find('.modal-body #pro_id').val(pro_id);
            modal.find('.modal-body #product_name').val(product_name);
        })
    </script>

@endsection
