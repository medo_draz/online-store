@extends('layouts.dashboard.master')
@section('css')
    <link href="{{URL::asset('dashboard/assets/plugins/owl-carousel/owl.carousel.css')}}" rel="stylesheet"/>
    <!-- Maps css -->
    <link href="{{URL::asset('dashboard/assets/plugins/jqvmap/jqvmap.min.css')}}" rel="stylesheet">
@section('title')
    Products - Admin Panel
@endsection
<!-- fgdsgd -->
@endsection
@section('page-header')
    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto">الاعدادات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/
                المنتجات</span>
            </div>
        </div>
    </div>
    <!-- breadcrumb -->
@endsection
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        {{--                        {{$product->category}}--}}
                    </div>
                </div>
                <div class="card-body">

                    <form
                        action="@if(isset($product)){{ route('admin.products.update',$product) }}@else{{ route('admin.products.store') }}@endif"
                        method="post" data-parsley-validate
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if(isset($product))
                            {{ method_field('put') }}
                        @else
                            {{ method_field('post') }}
                        @endif

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <input type="hidden" name="id"
                                           value="@if(isset($product)){{$product->id}}@endif">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Product
                                        Name : <span class="tx-danger">*</span></label>
                                    <input class="form-control" name="name"
                                           value="@if(isset($product)){{$product->name}}@endif" required
                                           type="text">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Category
                                        Name : <span class="tx-danger">*</span></label>
                                    <div class="parsley-select" id="slWrapper">
                                        <select class="form-control select2" data-parsley-class-handler="#slWrapper"
                                                data-parsley-errors-container="#slErrorContainer" required
                                                name="category_id">
                                            <option label="Choose one">
                                            </option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"
                                                        @if(isset($product) && $product->category->id == $category->id) selected @endif
                                                >
                                                    {{ $category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Description
                                        : <span class="tx-danger">*</span></label>
                                    <textarea class="form-control" name="desc" required placeholder="Description"
                                              rows="2">@if(isset($product)){{$product->desc}}@endif</textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Materials
                                        : </label>
                                    {{--                            <input class="form-control" name="desc" type="text">--}}
                                    <textarea class="form-control" name="materials" placeholder="Materials"
                                              rows="2">@if(isset($product)){{$product->materials}}@endif</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Sizes
                                        : <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="sizes[]" data-parsley-class-handler="#slWrapper"
                                            data-parsley-errors-container="#slErrorContainer" required multiple="multiple">
                                        <option value="S" @if(isset($product) && in_array("S",  $product_sizes)) selected @endif>S</option>
                                        <option value="M" @if(isset($product) && in_array("M",  $product_sizes)) selected @endif>M</option>
                                        <option value="L" @if(isset($product) && in_array("L",  $product_sizes)) selected @endif>L</option>
                                        <option value="XL" @if(isset($product) && in_array("XL",  $product_sizes)) selected @endif>XL</option>
                                        <option value="XXL" @if(isset($product) && in_array("XXL",  $product_sizes)) selected @endif>XXL</option>
                                        <option value="XXXL" @if(isset($product) && in_array("XXXL",  $product_sizes)) selected @endif>XXXL</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Sale
                                        Colors
                                        : <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="colors[]" data-parsley-class-handler="#slWrapper"
                                            data-parsley-errors-container="#slErrorContainer" required multiple="multiple">
                                        <option value="Black" @if(isset($product) && in_array("Black",  $product_colors)) selected @endif>Black</option>
                                        <option value="Green" @if(isset($product) && in_array("Green",  $product_colors)) selected @endif>Green</option>
                                        <option value="Red" @if(isset($product) && in_array("Red",  $product_colors)) selected @endif>Red</option>
                                        <option value="Blue" @if(isset($product) && in_array("Blue",  $product_colors)) selected @endif>Blue</option>
                                        <option value="White" @if(isset($product) && in_array("White",  $product_colors)) selected @endif>White</option>
                                        <option value="Grey" @if(isset($product) && in_array("Grey",  $product_colors)) selected @endif>Grey</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Price
                                        : <span class="tx-danger">*</span></label>
                                    <input type="number" class="form-control" placeholder="Price" required min="0"
                                           step=".01" value="@if(isset($product)){{$product->price}}@endif"
                                           name="price">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Sale
                                        Price
                                        : <span class="tx-danger">*</span></label>
                                    <div class="input-group mb-3">
                                        <input type="number" id="sale_price" class="form-control" required
                                               placeholder="Sale Price"
                                               value="@if(isset($product)){{$product->sale_price}}@endif" min="0"
                                               step=".01" name="sale_price">
                                        <div class="input-group-append">
                                                <span class="input-group-text">After Discount : <span
                                                        id="after_discount">@if(isset($product)){{$product->disc_price}}@endif</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Stock
                                        : <span class="tx-danger">*</span></label>
                                    <input type="number" class="form-control" placeholder="Stock" required min="0"
                                           name="stock" value="@if(isset($product)){{$product->stock}}@endif">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Discount
                                        : </label>
                                    <div class="input-group mb-3">
                                        <input type="number" id="discount" class="form-control"
                                               placeholder="Discount"
                                               min="0" name="discount"
                                               value="@if(isset($product)){{$product->discount}}@endif">
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label" style="width: 155px;">Image :
                                        <span
                                            class="tx-danger">*</span></label>
                                    <input type="file" name="image" style="min-height: 42px !important;"
                                           class="form-control image">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label" style="width: 155px;">Images
                                        : </label>
                                    <input type="file" name="images[]" style="min-height: 42px !important;" multiple
                                           class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    @if(isset($product))
                                        <img src="{{ asset('frontend/images/products/'.$product->image) }}"
                                             id="image-preview"
                                             style="width: 100px" class="img-thumbnail image-preview" alt="">
                                    @else
                                        <img src="{{ asset('frontend/images/products/default.jpg') }}"
                                             id="image-preview"
                                             style="width: 100px" class="img-thumbnail image-preview" alt="">
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label" style="width: 155px;">Status
                                        : </label>
                                    <label class="ckbox"><input type="checkbox"
                                                                name="status"
                                                                @if(isset($product) && $product->status == 1) checked
                                                                @endif value="1"><span>Publish</span></label>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            setTimeout(function () {
                $(".alert-danger").fadeOut(3000);
            }, 3000)

        })

        $("#sale_price").change(function () {
            console.log($(this).val())
            console.log($('#discount').val())
            $('#after_discount').text($(this).val())
            var sale_price = $(this).val()
            var discount = $('#discount').val()
            var disc = 0
            if (discount > 0) {
                disc = Number(sale_price) * (Number(discount) / 100)
                // $(this).val(Number(sale_price) - disc)
                $('#after_discount').text(Number(sale_price) - disc)
            }
        });
        $("#discount").change(function () {
            console.log($(this).val())
            console.log($('#sale_price').val())
            var discount = $(this).val()
            var sale_price = $('#sale_price').val()
            var disc = 0
            if (sale_price > 0) {
                disc = Number(sale_price) * (Number(discount) / 100)
                // $('#sale_price').val(Number(sale_price) - disc)
                $('#after_discount').text(Number(sale_price) - disc)
            }
        });
    </script>

@endsection
