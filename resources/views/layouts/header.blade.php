@php $method = request()->route()->uri; @endphp
<header class="">
    <!-- Header desktop -->
    <div class="container-menu-desktop">
        <!-- Topbar -->
        <div class="top-bar">
            <div class="content-topbar flex-sb-m h-full container">
                <div class="left-top-bar">
                    Free shipping for standard order over $100
                </div>

{{--                <div class="right-top-bar flex-w h-full">--}}
{{--                    <a href="#" class="flex-c-m trans-04 p-lr-25">--}}
{{--                        Help & FAQs--}}
{{--                    </a>--}}

{{--                    <a href="#" class="flex-c-m trans-04 p-lr-25">--}}
{{--                        My Account--}}
{{--                    </a>--}}

{{--                    <a href="#" class="flex-c-m trans-04 p-lr-25">--}}
{{--                        EN--}}
{{--                    </a>--}}

{{--                    <a href="#" class="flex-c-m trans-04 p-lr-25">--}}
{{--                        USD--}}
{{--                    </a>--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="wrap-menu-desktop @php if ($method !== '/') { echo 'how-shadow1';} @endphp">
            <nav class="limiter-menu-desktop container">
                <!-- Logo desktop -->
                <a href="{{ route('home') }}" class="logo">
                    <img src="{{ asset('frontend/images/icons/logo-01.png') }}" alt="IMG-LOGO">
                </a>

                <!-- Menu desktop -->
                <div class="menu-desktop">
                    <ul class="main-menu">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li>
                            <a href="{{ route('category','all') }}">Categories</a>
{{--                            <ul class="sub-menu">--}}
{{--                                @if(isset($categories))--}}
{{--                                    @foreach($categories as $category)--}}
{{--                                        <li><a href="index.html">{{ $category->name }}</a></li>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}
{{--                            </ul>--}}
                        </li>
{{--                        <li class="label1" data-label1="hot"><a href="shoping-cart.html">Features</a></li>--}}
                        <li><a href="{{ route('blog') }}">Blog</a></li>
                        <li><a href="{{ route('about') }}">About</a></li>
                        <li><a href="{{ route('contact') }}">Contact</a></li>
                    </ul>
                </div>

                <!-- Icon header -->
                <div class="wrap-icon-header flex-w flex-r-m h-full">
                    <div class="flex-c-m h-full p-r-5 bor6">
                        <div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 icon-header-noti  js-show-cart" data-type="card" id="icon-card"
                             data-notify="0">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                    </div>

                    <a href="#" class="icon-header-item cl2 hov-cl1 trans-04 p-l-15 p-r-11 icon-header-noti js-show-cart" data-type="favourite" id="icon-favourite" data-notify="{{count($favourites)}}">
                        <i class="zmdi zmdi-favorite-outline"></i>
                    </a>

                    <div class="flex-c-m h-full p-lr-19">
                        <div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 js-show-sidebar">
                            <i class="zmdi zmdi-menu"></i>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- Header Mobile -->
    <div class="wrap-header-mobile">
        <!-- Logo moblie -->
        <div class="logo-mobile">
            <a href="{{ route('home') }}"><img src="{{ asset('frontend/images/icons/logo-01.png') }}" alt="IMG-LOGO"></a>
        </div>

        <!-- Icon header -->
        <div class="wrap-icon-header flex-w flex-r-m h-full m-r-15">
            <div class="flex-c-m h-full p-r-5">
                <div class="icon-header-item cl2 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart"
                     data-notify="2">
                    <i class="zmdi zmdi-shopping-cart"></i>
                </div>
            </div>
        </div>

        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
        </div>
    </div>
    <!-- Menu Mobile -->
    <div class="menu-mobile">
        <ul class="main-menu-m">
            <li>
                <a href="{{ route('home') }}">Home</a>
                <span class="arrow-main-menu-m">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </span>
            </li>
            <li><a href="product.html">Shop</a></li>
{{--            <li><a href="shoping-cart.html" class="label1 rs1" data-label1="hot">Features</a></li>--}}
            <li><a href="blog.html">Blog</a></li>
            <li><a href="about.html">About</a></li>
            <li><a href="contact.html">Contact</a></li>
        </ul>
    </div>
    <!-- Modal Search -->
    <div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
        <button class="flex-c-m btn-hide-modal-search trans-04">
            <i class="zmdi zmdi-close"></i>
        </button>

        <form class="container-search-header">
            <div class="wrap-search-header">
                <input class="plh0" type="text" name="search" placeholder="Search...">

                <button class="flex-c-m trans-04">
                    <i class="zmdi zmdi-search"></i>
                </button>
            </div>
        </form>
    </div>
</header>
