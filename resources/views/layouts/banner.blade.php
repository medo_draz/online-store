<div class="sec-banner bg0 p-t-95 p-b-55">
    <div class="container">
        <div class="row">
            @if(isset($categories))
                @foreach($categories as $category)
                    <div class="col-md-6 p-b-30 @php if ($category->name == 'Women'|| $category->name == 'Men'){ echo ''; } else { echo 'col-lg-4'; }  @endphp m-lr-auto">
                        <!-- Block1 -->
                        <div class="block1 wrap-pic-w">
                            <img src="{{ asset('frontend/images/'. $category->image) }}" alt="IMG-BANNER">

                            <a href="{{ route('category',strtolower($category->name)) }}"
                               class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									{{ $category->name }}
								</span>

                                    <span class="block1-info stext-102 trans-04">
									New Trend
								</span>
                                </div>
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link stext-101 cl0 trans-09">
                                        Shop Now
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
