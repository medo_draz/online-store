<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/jquery/jquery-3.3.1.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('frontend/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/js/main.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/select2/select2.min.js') }}"></script>
<script>
    $(".js-select2").each(function () {
        $(this).select2({
            minimumResultsForSearch: 20,
            dropdownParent: $(this).next('.dropDownSelect2')
        });
    })
</script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('frontend/vendor/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/slick/slick.min.js') }}"></script>
<script src="{{ asset('frontend/js/slick-custom.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/parallax100/parallax100.js') }}"></script>
<script>
    $('.parallax100').parallax100();
</script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/MagnificPopup/jquery.magnific-popup.min.js') }}"></script>
<script>
    $('.gallery-lb').each(function () { // the containers for all your galleries
        $(this).magnificPopup({
            delegate: 'a', // the selector for gallery item
            type: 'image',
            gallery: {
                enabled: true
            },
            mainClass: 'mfp-fade'
        });
    });
</script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/isotope/isotope.pkgd.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/sweetalert/sweetalert.min.js') }}"></script>
<script>
    $('.js-addwish-b2').on('click', function (e) {
        e.preventDefault();
    });

    $('.js-addwish-b2').each(function () {
        var nameProduct = $(this).parent().parent().find('.js-name-b2').html();
        var prod_id = $(this).data('prod_id')

        $(this).on('click', function () {
            $.ajax({
                url: '/product/favourite/add',
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "prod_id": prod_id
                },
                success: function (data) {
                    // console.log(data);
                    let x = 0
                    if (data.type === 'add') {
                        x = Number($('#icon-favourite').attr('data-notify')) + 1
                        $('#icon-favourite').attr('data-notify', x)
                        $('#fav-'+prod_id).addClass('js-addedwish-b2');
                    } else {
                        x = Number($('#icon-favourite').attr('data-notify')) - 1
                        $('#icon-favourite').attr('data-notify', x)
                        $('#fav-'+prod_id).removeClass('js-addedwish-b2');
                    }
                    swal(nameProduct, data.message, "success");
                    // $(this).off('click');
                },
                error: function (data) {
                    // console.log(data.status);
                    if (data.status === 401) {
                        window.location.replace("/login")
                    }
                }
                // datatype: 'html'
            })
            // swal(nameProduct, "is added to wishlist !", "success");
            // $(this).addClass('js-addedwish-b2');
            // $(this).off('click');
        });
    });

    $('.js-addwish-detail').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.js-name-detail').html();

        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");

            $(this).addClass('js-addedwish-detail');
            $(this).off('click');
        });
    });

    /*---------------------------------------------*/

    $('.js-addcart-detail').each(function () {
        var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to cart !", "success");
        });
    });
</script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script>
    $('.js-pscroll').each(function () {
        $(this).css('position', 'relative');
        $(this).css('overflow', 'hidden');
        var ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            scrollingThreshold: 1000,
            wheelPropagation: false,
        });

        $(window).on('resize', function () {
            ps.update();
        })
    });
</script>
<!--===============================================================================================-->
<script src="{{ asset('frontend/js/main.js') }}"></script>
