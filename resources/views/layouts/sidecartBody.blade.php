<div class="header-cart-title flex-w flex-sb-m p-b-8">
    <span class="mtext-103 cl2">
        @if($type == 'favourite')
            Your Favourites
        @elseif($type == 'card')
            Your Cart
        @endif
    </span>
    <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
        <i class="zmdi zmdi-close"></i>
    </div>
</div>

<div class="header-cart-content flex-w js-pscroll">
    <ul class="header-cart-wrapitem w-full">
        @if(isset($products))
            @foreach($products as $product)
                <li class="header-cart-item flex-w flex-t m-b-12">
                    <div class="header-cart-item-img">
                        <img src="{{ asset('frontend/images/products/'.$product->product->image) }}" alt="IMG">
                    </div>

                    <div class="header-cart-item-txt p-t-8">
                        <a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
                            {{ $product->product->name }}
                        </a>

                        <span class="header-cart-item-info">
								{{ $product->product->sale_price }}
							</span>
                    </div>
                </li>
            @endforeach
        @endif
    </ul>

    <div class="w-full">
        @if($type == 'card')
            <div class="header-cart-total w-full p-tb-40">
                Total: $0
            </div>
        @endif
        <div class="header-cart-buttons flex-w w-full">
            @if($type == 'card')
                <a href="shoping-cart.html"
                   class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
                    View Cart
                </a>
                <a href="shoping-cart.html"
                   class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
                    Check Out
                </a>
            @endif
        </div>
    </div>
</div>

<script>
    $('.js-hide-cart').on('click', function () {
        $('.js-panel-cart').removeClass('show-header-cart');
    });
</script>
